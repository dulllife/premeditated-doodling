use std::env;
use std::fmt;

#[derive(Debug)]
struct Point {
    x: f64,
    y: f64,
    z: f64,
}

#[derive(Copy,Clone,Debug)]
enum Face {
    XPOSITIVE,
    XNEGATIVE,
    YPOSITIVE,
    YNEGATIVE,
    ZPOSITIVE,
    ZNEGATIVE,
}

impl fmt::Display for Face {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match *self {
            Face::XPOSITIVE => "XPOSITIVE",
            Face::XNEGATIVE => "XNEGATIVE",
            Face::YPOSITIVE => "YPOSITIVE",
            Face::YNEGATIVE => "YNEGATIVE",
            Face::ZPOSITIVE => "ZPOSITIVE",
            Face::ZNEGATIVE => "ZNEGATIVE",
        })
    }
}

fn face_to_int(f: Face) -> i64 {
    match f {
        Face::XPOSITIVE => 0,
        Face::XNEGATIVE => 1,
        Face::YPOSITIVE => 2,
        Face::YNEGATIVE => 3,
        Face::ZPOSITIVE => 4,
        Face::ZNEGATIVE => 5,
    }
}

fn get_uv(point: &Point, face: Face) -> (f64, f64) {
    match face {
        Face::XPOSITIVE => (point.y / point.x, point.z / point.x),
        Face::YPOSITIVE => (-point.x / point.y, point.z / point.y),
        Face::ZPOSITIVE => (-point.x / point.z, -point.y / point.z),
        Face::XNEGATIVE => (point.z / point.x, point.y / point.x),
        Face::YNEGATIVE => (point.z / point.y, -point.x / point.y),
        Face::ZNEGATIVE => (-point.y / point.z, -point.x / point.z),
    }
}

impl Point {
    fn xyz_to_face_uv(&self) -> (Face, f64, f64) {
        let face = self.face();
        let (u, v) = get_uv(&self, face);
        (face, u, v)
    }

    fn from_face_i_j(face: Face, i: i32, j: i32) -> i64 {
        return face_to_int(face) << 61;
    }

    fn face(&self) -> Face {
        let x = self.x.abs();
        let y = self.y.abs();
        let z = self.z.abs();

        if x > y {
            if x > z {
                if x > 0.0 {
                    Face::XPOSITIVE
                } else {
                    Face::XNEGATIVE
                }
            } else {
                if z > 0.0 {
                    Face::ZPOSITIVE
                } else {
                    Face::ZNEGATIVE
                }
            }
        } else {
            if y > z {
                if y > 0.0 {
                    Face::YPOSITIVE
                } else {
                    Face::YNEGATIVE
                }
            } else {
                if z > 0.0 {
                    Face::ZPOSITIVE
                } else {
                    Face::ZNEGATIVE
                }
            }
        }
    }
}

// Quadratic projection
fn uv_to_st(u_or_v: f64) -> f64 {
    if u_or_v >= 0.0 {
        0.5 * (1.0 + 3.0 * u_or_v).sqrt()
    } else {
        1.0 - 0.5 * (1.0 - 3.0 * u_or_v).sqrt()
    }
}

const PD_FACE_BITS: u32 = 3;
const PD_NUM_FACES: u32 = 6;
const PD_MAX_LEVEL: u32 = 30;
const PD_POSITION_BITS: u32 = 2 * PD_MAX_LEVEL + 1;
const LEAF_CELLS_MAX_COORDINATES: u32 = 1 << PD_MAX_LEVEL;

fn st_to_ij(s: f64) -> i32 {
    return (((LEAF_CELLS_MAX_COORDINATES as f64) * s) - 0.5)
        .round()
        .min((LEAF_CELLS_MAX_COORDINATES as f64) - 1.0)
        .max(0.0) as i32;
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        println!("Usage:\n\t./premeditated-doodling <latitude> <longitude>");
        return;
    }

    let x: f64 = args[1]
        .parse()
        .expect("./premeditated-doodling <latitude> <longitude> <altitude>");
    let y: f64 = args[2]
        .parse()
        .expect("./premeditated-doodling <latitude> <longitude> <altitude>");
    let z: f64 = args[3]
        .parse()
        .expect("./premeditated-doodling <latitude> <longitude> <altitude>");

    let point = Point {x, y, z};
    let face = point.face();
    println!("Face: {}", face);
    println!("Face int: {}", face_to_int(face));
    println!("From face i j: {}", Point::from_face_i_j(face, 1, 1));
    println!("From face i j in binary: {:b}", Point::from_face_i_j(face, 1, 1));
}
