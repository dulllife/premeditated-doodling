# On the Geometrical Modelling and Coordinate Systems used in Premeditated Doodling

## Geometrical Modelling

We assume that the Earth is a unit sphere. We put this sphere in a biunit cube
(a box centered on the origin whose faces are tangential to the unit sphere.
The Hilbert curve will be drawn on the faces of the biunit cube.

All coordinate systems are right-handed.

## The `(x, y, z)` coordinate space

In this space, the unit sphere is centered at the origin. In this space, to
project a point onto the unit sphere, simply normalize it.

## The `(face, u, v)` cube side space

This space describes the location of a point relative to the `u` and `v` axes
of a particular `face`. The cross product of the `u` and `v` axes of a
particular face is always a vector in the direction that the face is facing --
i.e., a vector perpendicular to the face and pointing away from the inside of
the cube. `u` and `v` are both in `[-1, 1]`. We do **NOT** directly determine
which cell a point in the cube side space lies in. We first go to the cell
space.

## The `(face, s, t)` cell space

This is a different coordinate space, whose points can be directly mapped to
cells.  We project to this space in order to reduce the variance in areas of
cells. We use a nonlinear transformation `u = f(s)` and `v = f(t)`. We can
choose `f` as one of three different transformations, all of which have
different accuracy and performance characteristics. `s` and `t` are both in the
interval `[0, 1]`.

## The `(face, i, j)` leaf cell coordinates

`i` and `j` are integers in the range `[0, (2^30) - 1]` that identify the leaf
cell on the given face. The faces are oriented such that the hilbert curve
connects continuously between adjoining faces.


